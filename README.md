# batch

Financial institutions have to perform Start of Day (SOD) process before it can carry out transactions for a specific date. Similarly, the End of Day (EOD) job must be run to process the transaction created during the day. When the scheduled EOD job 